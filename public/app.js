const studentsList = document.querySelector('#students-list');
const form = document.querySelector('#add-form');
// create element & render student
function showStudents(doc){
    let li = document.createElement('li');
    let emri = document.createElement('span');
    let mbiemri = document.createElement('span');
    let ID = document.createElement('span');
    let email = document.createElement('span');
    let qyteti = document.createElement('span');
    let fakulteti = document.createElement('span');
    let departamenti = document.createElement('span');

    li.setAttribute('data-id', doc.id);
    emri.textContent = doc.data().Emri;
    mbiemri.textContent = doc.data().Mbiemri;
    ID.textContent = doc.data().ID;
    email.textContent = doc.data().Email;
    fakulteti.textContent = doc.data().Fakulteti;
    qyteti.textContent = doc.data().Qyteti;
    departamenti.textContent = doc.data().Departamenti;
  
    li.appendChild(emri);
    li.appendChild(mbiemri);
    li.appendChild(ID);
    li.appendChild(email);
    li.appendChild(qyteti);
    li.appendChild(fakulteti);
    li.appendChild(departamenti);

    studentsList.appendChild(li);
}


// getting data
db.collection('students').get().then(snapshot => {
    snapshot.docs.forEach(doc => {
        showStudents(doc);
    });
});

//saving data

form.addEventListener('submit', (e) => {
    e.preventDefault();

    db.collection('students').add({
        Emri: form.emri.value,
        Mbiemri: form.mbiemri.value,
        ID: form.id.value,
        Email: form.email.value,
        Qyteti: form.qyteti.value,
        Fakulteti: form.fakulteti.value,
        Departamenti: form.departamenti.value,
    });
    form.emri.value = '';
    form.mbiemri.value = '';
    form.id.value = '';
    form.email.value = '';
    form.qyteti.value = '';
    form.fakulteti.value = '';
    form.departamenti.value='';
});
